import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Auth0Provider } from '@auth0/auth0-react';
import Routes from './routes';
import GlobalStyle from './styles/global';

const domain = process.env.REACT_APP_AYTH0_DOMAIN;
const clientID = process.env.REACT_APP_AYTH0_CLIENT_ID;

function App() {
  return (
    <Router>
      <Auth0Provider
        domain={domain}
        clientId={clientID}
        redirectUri={window.location.origin}
      >
        <Routes />
      </Auth0Provider>
      <GlobalStyle />
    </Router>
  );
}

export default App;
