import React, { useState, useRef } from 'react';
import { Form } from '@unform/web';
import * as Yup from 'yup';

import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import Input from '../Input/index';
import EmailIC from '../../assets/images/ic-email.svg';
import LockIC from '../../assets/images/ic-cadeado.svg';

import { Container } from './styles';

function FormIn() {
  const [submit, setSubmit] = useState(true);

  const formRef = useRef(null);

  /* Post dos dados do input */

  async function handleSubmit(data, { reset }) {
    try {
      const schema = Yup.object().shape({
        password: Yup.string().required(''),
        email: Yup.string().required('')
          .email('')
          .required(''),
      });
      await schema.validate(data, { abortEarly: false });
      // apiPost(data);
      formRef.current.setErrors({});

      setTimeout(() => {
        setSubmit(!submit);
      }, 0);
      reset();
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errorMessages = {};
        err.inner.forEach((error) => {
          errorMessages[error.path] = error.message;
        });
        formRef.current.setErrors(errorMessages);
      }
    }
  }

  function inputEvent() {
    const [visible, setVisible] = useState(true);

    const showPassword = (props) => {
      const passwordVisible = props;

      return (
        <>
          {passwordVisible
            ? (
              <AiFillEyeInvisible
                className="icon"
                size={18}
                color="gray"
                onClick={() => setVisible(!props)}
              />

            )
            : (
              <AiFillEye
                className="icon"
                size={18}
                color="gray"
                onClick={() => setVisible(!props)}
              />

            )}
        </>
      );
    };

    return (
      <Container>
        <div className="form">
          <Form ref={formRef} onSubmit={handleSubmit}>
            <div className="input-email">
              <img src={EmailIC} alt="emailIcon" />
              <Input name="email" placeholder="E-mail" />
            </div>
            <div className="input-password">
              <img src={LockIC} alt="lockIcon" />

              <Input name="password" placeholder="Senha" type={visible ? 'password' : 'text'} />
              {showPassword(visible)}

            </div>
            <button type="submit">ENTRAR</button>
          </Form>
        </div>
      </Container>
    );
  }

  return inputEvent();
}

export default FormIn;
