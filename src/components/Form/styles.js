import styled from 'styled-components';

export const Container = styled.div`
  @import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,400;0,700;1,300&display=swap');

  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  width: 100%;
  /*background-color: #eeecdb;*/

  @media only screen and (max-width: 323px) {
    display: flex;
    flex-direction: column;
    height: 100%;
  }

  @media only screen and (max-width: 750px) {
    width: 100%;
    display: flex;
    height: 146px;
  }


  form {
    display: flex;
    flex-direction:column;
    justify-content: space-between;
    align-items: center;


    @media only screen and (max-width: 323px) {
      display: flex;
      flex-direction: column;
      justify-content: left;
    }
  }

  button {
  width: 14.75rem;
  height: 2.563rem;
  margin: 2.563rem 0 0;
  border-radius: 3.9px;
  background-color: #57bbbc;
  border:none;
  color: #ffff;
  font-weight:bold;


    @media only screen and (max-width: 323px) {
      display: flex;
      width: 288px;
      height: 48px;
      background: #000000;
      border-radius: 5px;
      font-style: normal;
      font-weight: bold;
      font-size: 14px;
      line-height: 14px;
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      color: #ffff;
      border: none;
      margin-left: 0px;
    }
  }

  .input-email{
    display:flex;
    width: 100%;
    width: 280px;
    height: 48px;
    background-color: #eeecdb;
    margin-right: 8px;
    border:1px;
    border-bottom: 2px gray solid;
    padding: 15px 0px 0px 5px;
    align-items:center;

    img{
      margin-right:5px;
    }

  }
  .input-password{
    display:flex;
    width: 100%;
    width: 280px;
    height: 48px;
    background-color: #eeecdb;
    margin-right: 8px;
    border:1px;
    border-bottom: 2px gray solid;
    padding: 15px 0px 0px 5px;
    align-items:center;

    img{
      margin-right:5px;
    }
  }
  Input {
    flex:1;
    border:none;
    background-color: #eeecdb;


    ::placeholder {
      font-family:'Roboto', sans-serif;
      font-weight: bold;
      font-size: 13px;
      line-height: 14px;
      color: gray;
    }

    .input-password[invalid]{
      border:red;
    }
  }

  .erro_msg{
    margin-top:0px;
    padding-bottom: 45px;
  }
`;
export const Content = styled.div`
  @media only screen and (max-width: 742px) {
    display: flex;
    flex-direction: column;
  }

  span {
    display: flex;
    justify-content: space-between;
    position: absolute;
    bottom: 9px;
    color: #d7182a;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 12px;

  }
`;
export const ContentMsg = styled.div`
  .content_msg {
    width: 100%;
    max-width: 360px;
    display: block;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }
  .content_msg_done {
    width: 100%;
    display: block;
    align-items: center;
    text-align: center;
    justify-content: center;
    height: 26px;
    margin-bottom: 15px;


     }
  .content_button {
    display: flex;
    align-items: center;
    justify-content: center;

    button {
      display: flex;
      position: relative;
      width: 328px;
      height: 48px;
      background: #000000;
      border-radius: 5px;
      font-style: normal;
      font-weight: bold;
      font-size: 14px;
      line-height: 14px;
      display: flex;
      justify-content: center;
      align-items: center;
      text-align: center;
      color: #ffff;
      border: none;
      margin-left: 0px;

      @media only screen and (max-width: 323px) {
        width: 288px;
      }
    }
  }
`;
