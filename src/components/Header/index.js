import React from 'react';

import Logo from '../../assets/images/logo-nav.png';

import { Container } from './style';

export default function Header() {
  return (
    <Container>
      <div>
        <img src={Logo} alt="WhiteLogo" />
      </div>
    </Container>
  );
}
