import React from 'react';

import { Container } from './style';

export default function Loading() {
  return (
    <Container>
      <div className="modal-loading">
        <div className="lds-ring">
          <div />
          <div />
          <div />
          <div />
        </div>
      </div>
    </Container>
  );
}
