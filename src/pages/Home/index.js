import React, { useState } from 'react';

import { Container } from './styles';
import Logo from '../../assets/images/logo-home.png';
import FormIn from '../../components/Form/index';
import Loading from '../../components/Loading';

const Home = () => {
  // eslint-disable-next-line no-unused-vars
  const [loading, setLoading] = useState(false);

  return (
    <>
      <Container>
        <div className="container">
          {loading ? <Loading /> : null}
          <div className="container_content">
            <div className="content">
              <img src={Logo} alt="logo" className="logo" />
              <div className="content_title">
                <p>BEM-VINDO AO EMPRESAS</p>
              </div>
              <div className="content_subtitle">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                  sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam
                </p>
              </div>
            </div>
          </div>

          <div className="container_form">
            <FormIn />
          </div>
        </div>
      </Container>
    </>
  );
};

export default Home;
