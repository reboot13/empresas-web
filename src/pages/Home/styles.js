import styled from 'styled-components';

export const Container = styled.div`
  justify-content: center;
  align-items:center;
  width: 100%;
  min-width: 320px;
  height:100vh;



.container{
height:100%;
align-items:center;
display:flex;
justify-content:center;
flex-direction:column;


}

.logo{
  width:14.438rem;
  margin: 0px 0px 30px 0px;
}

.content {
    display: flex;
    flex-direction:column;
    justify-content: center;
    align-items:center;

  }
  .content_title {
    display: flex;
    justify-content: center;

    p {
    font-family: 'Roboto',sans-serif;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 16px;
    width: 120px;
    text-align: center;
    }
  }
  .content_subtitle{
    margin: 5px 0px 15px 0px;


    p {
      font-size:12px;
      font-family:'Roboto',sans-serif;
      text-align:center;
      max-width:280px;
    }
  }

`;
